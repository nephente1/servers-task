import * as React from 'react';
import { MainState } from './MainState';

export class AppState {
    readonly mainState: MainState;

    constructor() {
        this.mainState = new MainState();
    }

    static createForContext(): AppState {
        return new AppState();
    }
}

const AppContext = React.createContext(AppState.createForContext());
export const Provider = AppContext.Provider;

export const useAppStateContext = (): AppState => {
    return React.useContext(AppContext);
};
