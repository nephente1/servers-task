import { action, computed, makeAutoObservable, runInAction } from 'mobx';

export type ServerStatusTypes = "ONLINE" | "OFFLINE" | "REBOOTING";

export interface SingleDataResponseTypes {
    id: number;
    name: string;
    status: ServerStatusTypes;
    searchValue: string;
}

export class MainState {
    results: Array<SingleDataResponseTypes> = [];
    singleResult: SingleDataResponseTypes | null = null;
    refreshData: boolean = true;
    searchValue: string = '';
    selectedItem: number = 0;
    isOpenButtons: boolean = false;

    constructor() {
        makeAutoObservable(this);
    }

    @action loadData = async() => {
        try {
            const response = await fetch(`http://localhost:4454/servers`);
            const respJson = await response.json();
            runInAction(() => { this.results = respJson });
        }
        catch(err){
            console.log('failed fetch')
        }
        runInAction(() => { this.refreshData = false });
    }

    @action loadSingleData = (id: number) => {
        const loadSingleServer = async() => {
            const response = await fetch(`http://localhost:4454/servers/${id}` );
            const respJson = await response.json();
            runInAction(() => { this.singleResult = respJson });
        }

        runInAction(() => { this.singleResult = null });

        const refresh = setInterval(() => {
            runInAction(() => { this.refreshData = true });
            loadSingleServer();

            if (this.getSingleData !== null && this.getSingleData.status === 'ONLINE') {
                clearInterval(refresh);
            }
        }, 1000);
    }

    @computed get getSingleData(): SingleDataResponseTypes | null {
        if (this.singleResult !== null) {
            return this.singleResult;
        }
        return null;
    }

    @action handleOffClick = (id: number): void => {
        const settings = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ id: `${id}`, status: 'OFFLINE'})
        }

        fetch(`http://localhost:4454/servers/${id}/off`, settings);
        runInAction(() => { this.refreshData = true });
    }

    @action handleOnClick = (id: number) => {
        const settings = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ id: `${id}`, status: 'ONLINE'})
        }

        fetch(`http://localhost:4454/servers/${id}/on`, settings);
        runInAction(() => { this.refreshData = true });
    }

    @action handleRebootOnClick = (id: number) => {
        const settings = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ id: `${id}`, status: 'REBOOTING'})
        }

        fetch(`http://localhost:4454/servers/${id}/reboot`, settings);
        runInAction(() => { this.refreshData = true });
        this.loadSingleData(id);
    }

    onSearchWord = (event: React.ChangeEvent<HTMLInputElement>): void => {
       this.searchValue = event.currentTarget.value;
    };

    @computed get dataToShow(): Array<SingleDataResponseTypes> | null {
        if (this.results.length > 0){
            const outFilteredData = this.results.filter((item) => {
                const conditionSearchString = this.searchValue.length > 0  ? item.name.toLowerCase().indexOf(this.searchValue.toLowerCase()) !== -1 : true;
                const outData = conditionSearchString;
                return outData;
            });
            return outFilteredData;
        }
        return null;
    }

    @action selectItem = (id: number): void => {
        this.selectedItem = id;
        this.isOpenButtons = true;
    }

    @action closeButtons(): void {
        this.isOpenButtons = false;
    }

}
