
import React from 'react';
import { shallow } from 'enzyme';
import { ServerStatus } from '../src/components/ServerStatus/ServerStatus';


describe('ServerStatus component', () => {

  it('should render', () => {
    const component = <ServerStatus />

    const tree = renderer.create(component).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('ServerStatus should show REBOOTING text when received such props', () => {
      const props = {
          status: 'REBOOTING'
      }
      const component = shallow(<ServerStatus {...props}/>);
      expect(component.find('p').text()).toEqual('REBOOTING...');
  })

});



// npm install --save-dev jest-styled-components
// npm i react-test-renderer