import styled from 'styled-components';

export const SelectCircle = styled('div')`
    position: relative;
    width: 37px;
    height: 37px;
    background: none;
    border-radius: 50%;
    display: flex;
    align-content: center;
    align-items: center;
    justify-content: center;
    margin: 0 29px 0 0;
    &:hover {
        background: #F2F3F6;
        cursor: pointer;
    }
`;

export const Dots = styled('div')`
position: relative;
    background: #A9AEC1;
    width: 4px;
    height: 4px;
    border-radius: 50%;
    &::after {
        content:'';
        display: block;
        background: #A9AEC1;
        width: 4px;
        height: 4px;
        border-radius: 50%;
        position: absolute;
        left: 7px;
    }
    &::before {
        content:'';
        display: block;
        background: #A9AEC1;
        width: 4px;
        height: 4px;
        border-radius: 50%;
        position: absolute;
        right: 7px;
    }
`;

export const ManageButtonsWrapper = styled('div')<{isVisible: boolean}>`
    position: absolute;
    right: -30px;
    top: -10px;
    display: ${props => props.isVisible ? 'block' : 'none'};
    z-index: 2;
    box-shadow: 0 0 4px 2px #cfcfcf;
`;

export const Button = styled('button')`
    width: 137px;
    font-weight: 600;
    text-transform: capitalize;
    padding: 21px 28px;
    background: #fff;
    border: none;
    outline: none;
    line-height: 1;
    &:hover {
        background: #F2F3F6;
        cursor: pointer;
    }
`;
