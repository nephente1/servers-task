import { observer } from 'mobx-react-lite';
import React from 'react';
import { useAppStateContext } from '../../appState/AppState';
import { Button, ManageButtonsWrapper } from './ManageSelectCircle.styles';

interface PropsType {
    id: number;
    status: string;
}

export const ManageButtons = observer((props: PropsType) => {
    const appState = useAppStateContext();
    const { id, status } = props;

    const handleOffClick = () => {
        appState.mainState.handleOffClick(id);
        appState.mainState.closeButtons();
    }

    const handleOnClick = () => {
        appState.mainState.handleOnClick(id);
        appState.mainState.closeButtons();
    }

    const isVisible = id === appState.mainState.selectedItem && appState.mainState.isOpenButtons;
    const online = React.useMemo(() => status === 'ONLINE', [status === 'ONLINE']);
    const offline = React.useMemo(() => status === 'OFFLINE', [status === 'OFFLINE']);

    return(
        <ManageButtonsWrapper isVisible={isVisible}>
            { online ? <Button onClick={handleOffClick}>turn off</Button> : null }
            { offline ? <Button onClick={handleOnClick}>turn on</Button> : null }
            { online ? <Button onClick={() => appState.mainState.handleRebootOnClick(id)}>reboot</Button> : null }
        </ManageButtonsWrapper>
    )
});
