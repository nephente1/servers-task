import React from 'react';
import { observer } from 'mobx-react-lite';
import { useAppStateContext } from '../../appState/AppState';
import { ManageButtons } from './ManageButtons';
import { SelectCircle, Dots } from './ManageSelectCircle.styles';

interface PropsType {
    id: number,
    status: string
}

export const ManageSelectCircle = observer((props: PropsType) => {
    const { id, status } = props;
    const appState = useAppStateContext();

    const renderButtons = () => {
        appState.mainState.selectItem(id);
        appState.mainState.isOpenButtons = true;
    }

    return(
        <SelectCircle onClick={renderButtons}>
            <Dots />
            <ManageButtons id={id} status={status}/>
        </SelectCircle>
    )
});
