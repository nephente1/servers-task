import styled from 'styled-components';

export const ContainerHeaderWrapper = styled('div')`
    display: flex;
    margin: 42px 0 0 0;
    justify-content: space-between;
    height: 60px;
    width: 100%;
`;

export const Title = styled('h2')`
    color: #494E61;
    font-size: 21px;
    font-weight: 600;
    margin: 0 0 11px 0;
`;

export const ServersInfo = styled('p')`
    color: #494E61;
    line-height: 0;
    margin: 0;
    font-size: 15px;
`;
