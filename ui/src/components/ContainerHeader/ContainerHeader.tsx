import { observer } from 'mobx-react-lite';
import React from 'react';
import { useAppStateContext } from '../../appState/AppState';
import { SearchComponent } from '../SearchComponent/SearchComponent';
import { ContainerHeaderWrapper, Title, ServersInfo } from './ContainerHeader.styles';

export const ContainerHeader = observer(() => {
    const appState = useAppStateContext();

    return(
        <ContainerHeaderWrapper>
            <div>
                <Title>Servers</Title>
                <ServersInfo>Number of servers: {appState.mainState.results.length}</ServersInfo>
            </div>
            <SearchComponent onChange={appState.mainState.onSearchWord} />
        </ContainerHeaderWrapper>
    )
})
