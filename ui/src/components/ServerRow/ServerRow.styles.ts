import styled from 'styled-components';

export const ServerRowWrapper = styled('div')`
    display: flex;
    height: 57px;
    align-items: center;
    position: relative;
    justify-content: space-between;
    border-bottom: 2px solid #F2F3F6;
`;

export const ServerNameAndStatus = styled('div')`
    display: flex;
    flex: 0 1 356px;
    margin: 0 0 0 42px;
    justify-content: space-between;
`;

export const ServerName = styled('p')`
    font-size: 13px;
    font-weight: 600;
    color: #494E61;
`;
