import React from 'react';
import { ServerStatusTypes } from '../../appState/MainState';
import { ManageSelectCircle } from '../ManageSelectCircle/ManageSelectCircle';
import { ServerStatus } from '../ServerStatus/ServerStatus';
import { ServerRowWrapper, ServerName, ServerNameAndStatus } from './ServerRow.styles';

interface ServerRowPropsType {
    name: string,
    status: ServerStatusTypes,
    id: number
}

export const ServerRow = (props: ServerRowPropsType ) => {
    const { name, status, id } = props;

    return (
        <ServerRowWrapper>
            <ServerNameAndStatus>
                <ServerName>{name}</ServerName>
                <ServerStatus status={status}/>
            </ServerNameAndStatus>
            <ManageSelectCircle status={status} id={id} />
        </ServerRowWrapper>
    )
}

