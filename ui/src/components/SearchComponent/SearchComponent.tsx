import { observer } from 'mobx-react-lite';
import React from 'react';
import { SearchInputWrapper, SearchInput, SearchIconWrapper } from './SearchComponent.styles';


interface SearchComponentPropsType {
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
}

export const SearchComponent = observer((props: SearchComponentPropsType) => {
    return (
        <SearchInputWrapper>
            <SearchIconWrapper />
            <SearchInput
                type="text"
                placeholder="Search"
                onChange={props.onChange} />
        </SearchInputWrapper>
    );
});
