import styled from 'styled-components';
import { SearchIcon } from '../../assets/SearchIcon';

export const SearchInputWrapper = styled('div')`
    position: relative;
    width: 263px;
    display: flex;
    height: 38px;
    padding-left: 49px;
    border: 2px solid #D4D7E1;
    border-radius: 30px;
`;

export const SearchInput = styled('input')`
    width: 80%;
    background-color: #F2F3F6;
    outline: none;
    border: none;
    &::placeholder {
        color: #A9AEC1;
        size: 14px;
        font-weight: 600;
    }
`;

export const SearchIconWrapper = styled(SearchIcon)`
    position: absolute;
    width: 16px;
    left: 14px;
    top: 0;
    bottom: 0;
    margin: auto;
`;
