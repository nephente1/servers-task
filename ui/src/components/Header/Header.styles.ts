import styled from 'styled-components';

export const HeaderWrapper = styled('header')`
    width: 100%;
    height: 73px;
    background: #494E61;
    color: #fff;
    display: flex;
    align-items: center;
`;

export const LogoWrapper = styled('div')`
    height: 21px;
    padding: 0 31px 0 27px;
    border-right: 2px solid #757B8F;
    font-size: 14px;
    color: #fff;
    font-weight: bold;
    display: flex;
`;

export const LogoCircle = styled('div')`
    width: 17px;
    height: 17px;
    border: 2px solid #7D82F7;
    border-radius: 50%;
    margin: 0 12px 0 0;
`;
