import React from 'react';
import { HeaderWrapper, LogoCircle, LogoWrapper } from './Header.styles';
import { observer } from 'mobx-react-lite';
import { useAppStateContext } from '../../appState/AppState';
import { Overlay } from '../App/App.styles';

export const Header = observer(() => {
    const appState = useAppStateContext();
    return(
        <>
        <HeaderWrapper>
            <LogoWrapper>
                <LogoCircle />
                <span>Recruitment task</span>
            </LogoWrapper>
        </HeaderWrapper>
        { appState.mainState.isOpenButtons ? <Overlay className="Overlay" onClick={() => appState.mainState.closeButtons()} /> : null }
        </>
    )
});
