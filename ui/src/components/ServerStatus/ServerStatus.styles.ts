import styled from 'styled-components';

export const ServerStatusText = styled('p')<{color: string}>`
    font-size: 12px;
    font-weight: 600;
    color: ${props => props.color};
    text-transform: uppercase;
`;

export const ServerStatusWrapper = styled('div')`
    position: relative;
    display: flex;
    width: 78px;
    justify-content: left;
    align-items: center;
`;

export const Close = styled('div')`
    margin: -10px 10px 0 2px;;
    &:before{
        position: absolute;
        content: '';
        height: 11px;
        width: 2px;
        background-color: #EA5885;
        border-radius: 4px;
        transform: rotate(45deg);
    }
    &:after{
        position: absolute;
        content: '';
        height: 11px;
        width: 2px;
        background-color: #EA5885;
        border-radius: 4px;
        transform: rotate(-45deg);
    }
`;

export const CircleDot = styled('div')`
    width: 7px;
    height: 7px;
    border-radius: 50%;
    background: #33CAD4;
    margin: 2px 7px 0 0;
`;
