import { observer } from 'mobx-react-lite';
import React from 'react';
import { CircleDot, Close, ServerStatusText, ServerStatusWrapper } from './ServerStatus.styles';

interface ServerStatusPropsType {
    status: string
}

export const ServerStatus = observer((props: ServerStatusPropsType) => {
    const {status} = props;

    const getStatus = (status: string): string | null => {
        switch (status) {
            case 'ONLINE':
                return 'ONLINE';
            case 'OFFLINE':
                return 'OFFLINE';
            case 'REBOOTING':
                return 'REBOOTING...';
            default:
                return null;
        }
    }

    const getColor = (status: string): string => {
        switch (status) {
            case 'ONLINE':
                return '#24A1A9';
            case 'OFFLINE':
                return '#494E61';
            case 'REBOOTING':
                return '#9CA7D3';
            default:
                return '';
        }
    }

    return(
        <ServerStatusWrapper>
            { props.status === 'OFFLINE' ? <Close /> : null }
            { props.status === 'ONLINE' ? <CircleDot /> : null }
            <ServerStatusText color={getColor(status)}>{getStatus(status)}</ServerStatusText>
        </ServerStatusWrapper>

    )
});
