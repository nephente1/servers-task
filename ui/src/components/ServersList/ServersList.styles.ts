import styled from 'styled-components';
import { ServerNameAndStatus } from '../ServerRow/ServerRow.styles';

export const ServersContainer = styled('div')`
    width: 100%;
    background: #fff;
    border: 2px solid #EDEDF0;
`;

export const ServersHeaderWrapper = styled('div')`
    height: 74px;
    border-bottom: 2px solid #F2F3F6;
    display: flex;
    align-items: center;
`;

export const HeaderTitle = styled('div')`
    text-transform: uppercase;
    font-weight: 600;
    color: #9CA7D3;
    font-size: 14px;
    &:first-of-type {
        margin-right: 247px;
    }
`;

export const ServerNameAndStatusHeader = styled(ServerNameAndStatus)`
    justify-content: flex-start;
`;
