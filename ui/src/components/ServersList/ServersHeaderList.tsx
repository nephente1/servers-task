import React from 'react';
import { ServersHeaderWrapper, HeaderTitle, ServerNameAndStatusHeader } from './ServersList.styles';


export const ServersHeaderList = () => {
    return (
        <ServersHeaderWrapper>
            <ServerNameAndStatusHeader>
                <HeaderTitle>Name</HeaderTitle>
                <HeaderTitle>Status</HeaderTitle>
            </ServerNameAndStatusHeader>
        </ServersHeaderWrapper>
    )
};
