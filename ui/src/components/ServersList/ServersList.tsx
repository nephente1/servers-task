import { observer } from 'mobx-react-lite';
import React from 'react';
import { useAppStateContext } from '../../appState/AppState';
import { ServerRow } from '../ServerRow/ServerRow';
import { ServersHeaderList } from './ServersHeaderList';
import { ServersContainer } from './ServersList.styles';

export const ServersList = observer(() => {
    const appState = useAppStateContext();
    const data = appState.mainState.dataToShow;

    React.useEffect ( () => {
        appState.mainState.loadData();
    }, [appState.mainState, appState.mainState.refreshData]);


    if (data === null){
        return null
    }

    const out = data.map( el =>
        <ServerRow
            key={el.id}
            name={el.name}
            status={el.status}
            id={el.id}
        />
    )

    return (
        <ServersContainer>
            <ServersHeaderList />
            {out}
        </ServersContainer>
    );
});

