// https://bitbucket.org/acaisoft/recruitment-reactjs/src/master/
// https://projects.invisionapp.com/d/main?origin=v7#/console/15730078/326594382/preview?scrollOffset=125
import React from 'react';
import { observer } from 'mobx-react-lite';
import { Provider, AppState } from '../../appState/AppState';
import { AppWrapper, Container } from './App.styles';
import { ContainerHeader } from '../ContainerHeader/ContainerHeader';
import { Header } from '../Header/Header';
import { ServersList } from '../ServersList/ServersList';


const App = observer(() => {
    const appStore = new AppState();

    return (
        <AppWrapper>
            <Provider value={appStore}>
                <Header />
                <Container>
                    <ContainerHeader />
                    <ServersList />
                </Container>
            </Provider>
        </AppWrapper>
    );
})

export default App;

