import styled from 'styled-components';

export const AppWrapper = styled('div')`
    position: relative;
    background-color: #F2F3F6;
    min-height: 100vh;
    color: #494E61;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const Container = styled('div')`
    margin-bottom: 125px;
    width: 100%;
    max-width: 1104px
`;

export const Overlay = styled('div')`
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 1;
`;

